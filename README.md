This is a program for finding all the ways to tile a given shape with given 
tiles.
To do this, enter the shape to be tiled in the shape file, like so:
```
xxxxx
xxxxx
xxxxx
xxxxx
xxxxx
```
Any character will do to draw the shape.
Enter the tiles separated by newlines in the tiles file, like so:
```
 xx
xx
 x

xxxx
x

xxxxx

xxx
xx

xx
 xxx

x
xxx
x

xx
x
xx

xxx
x
x

 xx
xx
x

 x
xxx
 x

xxxx
 x

x
xxx
  x
```
Then, run `./tile`. `tile` takes one argument, which is a string of characters.
For example:

`./tile frq`  
`f` - enables piece flipping [flips top to bottom]  
`r` - enables piece rotation  
`q` - enables a piece quota to be entered

Piece quotas are used when only a limited number of certain pieces exist.
When this quota is enabled, it will first ask you how many of each piece you
need. Type it in and hit enter, or leave it blank to refrain from imposing
a limit on that piece.
There is another argument character to streamline this.

`./tile frQ`  
`Q` - enables piece quota, with only one piece of each type allowed

`Q` will trump `q` when both are included.